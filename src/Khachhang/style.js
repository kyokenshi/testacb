import styled from "styled-components";
import "antd/dist/antd.css";

export const Appp = styled.div`
  /* background-image: url('/background-homepage.png');
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  height :235px; */
  height: 100%;
  /* padding: 16px; */
  max-width: 576px;
  margin: auto;
  padding-top: 0;
  //   background-color: #f5f6f7;
`;

export const HomeLayout = styled.div`
  background-image: url("/background-homepage.png");
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  height: 235px;
`;
export const HomeContainer = styled.div`
  min-height: 168px;
  display: flex;
  position: relative;
  .header {
    width: 100%;
    padding-top: 32px;
    padding-bottom: 32px;
    .header-infor {
      display: flex;
      justify-content: space-between;
      align-items: flex-start;
      padding: 0 16px;

      .infor-trai {
        display: flex;
        .text1 {
          display: flex;
          flex-direction: column;
          margin-left: 10px;
        }
      }
      .infor-phai {
        margin-right: 20px;
      }
    }
  }

  .header-order {
    background: #fafbfc;
    border: 1px solid #e6e8eb;
    border-radius: 8px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: -30%;
    left: 50%;
    transform: translate(-50%);
    padding: 8px 20px;

    width: calc(100% - 32px);
    /* .ant-btn {
      margin: 20px;
      color: #ffffff;
    border-color: #0073C4;
    background-color: #0073C4;
    text-shadow: none;
    border-radius: 4px;
    font-weight: 600;
    } */
  }
`;

export const Section = styled.div`
  background-color: #ffffff;
  margin-top: 8px;
  padding: 0 16px;
  width: 100%;
  max-width: 576px;

  margin-top: 18%;

  .text-section {
    color: #42526e;
    font-size: 16px;
    font-weight: 600;
    line-height: 24px;
  }
  .home-card-white {
    background: #fafbfc;
    border: 1px solid #e6e8eb;
    box-sizing: border-box;
    border-radius: 8px;
    padding: 24px 16px;
    display: flex;
    margin-top: 15px;
    .text-list {
      display: flex;
      flex-direction: column;
      margin-left: 10px;
    }
  }
`;
export const SectionHT = styled.div`
  padding-top: 32px;
  padding-bottom: 32px;
  .text-hoithao {
    padding: 0 16px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

export const SectionTT = styled.div`
  padding: 0 16px;
  .text-tt {
    margin-bottom: 10px;
  }
  .imgtt {
    width: 100%;
  }
`;
export const Tabwrap = styled.div`
  padding: 0 16px;
  display: flex;
  justify-content: space-between;
  position: fixed;

  bottom: 0;
  left: 50%;
  background: #fff;
  box-shadow: 0 -2px 6px rgba(133, 135, 137, 0.1);
  z-index: 999;
  width: 100%;
  max-width: 544px;
  transform: translate(-50%);
`;
