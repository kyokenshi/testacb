import React, { useState, useEffect } from "react";
import { Avatar } from "antd";

// import link

import { useHistory } from "react-router-dom";

// import style
import {
  Appp,
  HomeLayout,
  HomeContainer,
  Section,
  SectionHT,
  SectionTT,
  Tabwrap,
} from "./style";
// import icon
import { BellOutlined } from "@ant-design/icons";

// component
import CardBS from "../components/BSCard";
import ButtonBS from "../components/ButtonBS";
import DrawerComponent from "../components/Drawer/Drawer";
import DrawerCustom from "../components/Drawer/Customerdetails/Customerdetail";
import BSCarousel from "../components/BSCarousel";

// hình ảnh
import homeheader from "../assets/images/home-header.png";
import listorder from "../assets/images/list-order.png";
import promotion from "../assets/images/promotion.png";
import next from "../assets/images/next.png";
import CardTT from "../assets/images/cardtt.png";
import trangchu from "../assets/images/trangchu.png";
import dathang from "../assets/images/dathang.png";
import hoithao from "../assets/images/hoithao.png";
import tintuc from "../assets/images/tintuc.png";
///api
import axios from "axios";

function Khachhang(props) {
  /////////////// click drawer
  const [visible, setVisible] = useState(false);
  const [open, setOpen] = useState(false);
  // tao bien info api
  const [info, setInfo] = useState({});
  const handleClickAvatar = () => {
    setOpen(true);
  };
  const token = localStorage.getItem("token");

  const handleClickNoti = () => {
    setVisible(true);
  };
  const [order, setoder] = useState([]);
  /////////
  let history = useHistory();
  ////////////
  function handleClickEvent() {
    console.log("object");
    history.push("/events");
  }
  function handleClickPromotion() {
    console.log("object");
    history.push("/promotion");
  }

  function handleClickManagement() {
    // console.log("object");
    history.push("/management");
  }
  ////////

  //api login
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      props.history && props.history.push("/login");
    }
  });

  /// api lay info
  useEffect(() => {
    const userInfo = JSON.parse(localStorage.getItem("userInfo"));
    setInfo(userInfo);
    // console.log(userInfo);
  }, []);

  const getOrder = () => {
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url: "cms/dashboardCustomer",
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setoder(res.data.data);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };
  useEffect(() => {
    getOrder();
  }, []);

  // useEffect(() => {
  //   async function
  //   return () => {
  //     cleanup
  //   }
  // }, [])

  return (
    <Appp>
      <HomeLayout>
        <HomeContainer>
          <div className="header">
            <div className="header-infor">
              <div className="infor-trai">
                <span>
                  <Avatar size={48} src="" onClick={handleClickAvatar} />
                </span>
                <div className="text1">
                  <span>Xin chào,</span>
                  <h5>{info.fullName}</h5>
                </div>
              </div>
              <div className="infor-phai">
                <BellOutlined onClick={handleClickNoti} />
              </div>
            </div>
            <div className="header-order">
              <img src={homeheader}></img>
              <ButtonBS />
            </div>
          </div>
        </HomeContainer>

        <Section>
          <div className="text-section">Quản Lý Đơn Hàng</div>
          <div className="home-card-white" onClick={handleClickManagement}>
            <div className="home-card-white-chtiet">
              <img src={listorder}></img>
            </div>
            <div className="text-list">
              <span>Quản lý đơn hàng </span>
              <span>{order.totalOders} Đơn hàng</span>
            </div>
          </div>
          <div className="home-card-white" onClick={handleClickPromotion}>
            <div className="home-card-white-chtiet">
              <img src={promotion}></img>
            </div>
            <div className="text-list">
              <span>Danh Sách Đơn Hàng</span>
              <span>{order.totalPromotion} Khuyến mãi</span>
            </div>
          </div>
        </Section>

        <SectionHT>
          <div className="section-hoithao">
            <div className="text-hoithao" onClick={handleClickEvent}>
              <span>Hội thảo</span>
              <img src={next}></img>
            </div>
            <BSCarousel>
              <CardBS openEventDetail={() => {}} />
            </BSCarousel>
          </div>
        </SectionHT>
        <SectionTT>
          <div className="text-tt">
            <span>Tin Tức</span>
          </div>
          <div>
            <img src={CardTT} alt="" className="imgtt" />
          </div>
        </SectionTT>
        <Tabwrap>
          <a href="#">
            <img src={trangchu}></img>
          </a>
          <a href="#">
            <img src={dathang}></img>
          </a>
          <a href="#">
            <img src={hoithao}></img>
          </a>
          <a href="#">
            <img src={tintuc}></img>
          </a>
        </Tabwrap>
      </HomeLayout>

      <DrawerComponent visible={visible} onClose={() => setVisible(false)} />

      <DrawerCustom open={open} onClose={() => setOpen(false)} data={[]} />
    </Appp>
  );
}

export default Khachhang;
