import React, { useState } from "react";

import LOGIN from "../assets/images/logologin.png";
import { LoginLayuot, LoginContainer, FormLogin, LoginAcount } from "./style";
import { Form, Input, Button, Checkbox } from "antd";
import axios from "axios";

function Login(props) {
  const [data, setData] = useState({
    phone: "",
    password: "",
  });

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      // [] la` key
      [name]: value,
    });
  };

  console.log(data);

  const handleSubmit = () => {
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/cms",
      method: "post",
      url: "user/login_by_phone_number",
      data: data,
    })
      .then((res) => {
        console.log(res.data.data.profile);
        // luu token vao local storge
        localStorage.setItem("token", res.data.data.token);
        // luu token vao local story , doi profile sang dang json string
        localStorage.setItem("userInfo", JSON.stringify(res.data.data.profile));

        props.history.push("/");
      })
      .catch((error) => {
        //api loi handle here!
      });
  };

  return (
    <LoginLayuot>
      <LoginContainer>
        <div className="login-logo">
          <img src={LOGIN} alt="login-logo"></img>
        </div>
      </LoginContainer>
      <FormLogin>
        <Form className="frm-container">
          <Form.Item
            className="formlayuot"
            label="Số điện thoại"
            rules={[
              {
                required: true,
                message: "Nhập số điện thoại!",
              },
            ]}
          >
            <Input
              className="inputs"
              name="phone"
              value={data.phone}
              onChange={handleOnChange}
            />
          </Form.Item>

          <Form.Item
            className="formlayuot"
            label="Mật Khẩu"
            rules={[
              {
                required: true,
                message: "Nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password
              className="inputs"
              name="password"
              value={data.password}
              onChange={handleOnChange}
            />
          </Form.Item>

          <Form.Item name="remember" valuePropName="checked">
            <div className="rememberforgot">
              <Checkbox>Remember me</Checkbox>
              <Button className="forgot">Quên mật khẩu</Button>
            </div>
          </Form.Item>

          <Form.Item>
            <Button
              className="formbutton"
              type="primary"
              htmlType="submit"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
        <LoginAcount>
          <span className="text">bạn chưa có tài khoản</span>
          <span className="buttontext">
            <a href="#">Tạo tài khoản</a>
          </span>
        </LoginAcount>
      </FormLogin>
    </LoginLayuot>
  );
}

export default Login;
