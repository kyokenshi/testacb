import styled from "styled-components";

export const Section = styled.div`
  .promotion-detail {
    height: calc(100vh - 56px);
    padding: 16px;
    .promotion-content {
      margin-bottom: 16px;
      .title {
        font-style: normal;
        font-weight: 600;
        font-size: 12px;
        color: #172b4d;
        margin-bottom: 4px;
        span {
          font-weight: normal !important;
          color: #6b778c !important;
        }
      }
      .description {
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        color: #6b778c;
        span {
          color: #172b4d !important;
          font-weight: 600 !important;
        }
      }
    }
  }
`;
