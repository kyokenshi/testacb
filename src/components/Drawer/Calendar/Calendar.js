import React, { useEffect } from "react";

import { Header, StyleWrapDrawer } from "../style";
import back from "../../../assets/images/back.png";
import { Calendarcontent, CalendarDate, StyleSelect } from "../Calendar/style";
import { Calendar, Select, Radio, Col, Row, Typography } from "antd";

function onPanelChange(value, mode) {
  console.log(value, mode);
}

function Calendarer({ eventdrw, onClose }) {
  function renderTitle() {
    return (
      <Header>
        <div className="title">
          <span>Thông báo</span>
        </div>
        <img src={back} className="imgback" onClick={onClose}></img>
      </Header>
    );
  }

  return (
    <StyleWrapDrawer
      title={renderTitle()}
      placement="right"
      closable={false}
      onClose={onClose}
      visible={eventdrw}
      width="100%"
    >
      <Calendarcontent>
        <CalendarDate>
          <h4>Lịch hội thảo đã đăng ký </h4>
          <div className="site-calendar-demo-card">
            <Calendar
              fullscreen={false}
              headerRender={({ value, type, onChange }) => {
                const start = 0;
                const end = 12;
                const monthOptions = [];

                const current = value.clone();
                const localeData = value.localeData();
                console.log("value", localeData.monthsShort(current));

                const months = [];
                for (let i = 0; i < 12; i++) {
                  current.month(i);
                  months.push(localeData.monthsShort(current));
                }

                for (let index = start; index < end; index++) {
                  monthOptions.push(
                    <Select.Option className="month-item" key={`${index}`}>
                      {months[index]}
                    </Select.Option>
                  );
                }
                const month = value.month();

                const year = value.year();
                const options = [];
                for (let i = year - 10; i < year + 10; i += 1) {
                  options.push(
                    <Select.Option key={i} value={i} className="year-item">
                      {i}
                    </Select.Option>
                  );
                }
                return (
                  <div style={{ padding: 16 }}>
                    <Row gutter={20}>
                      <Col span={12}>
                        <StyleSelect
                          size="small"
                          dropdownMatchSelectWidth={false}
                          value={String(month)}
                          onChange={(selectedMonth) => {
                            const newValue = value.clone();
                            newValue.month(parseInt(selectedMonth, 10));
                            onChange(newValue);
                          }}
                        >
                          {monthOptions}
                        </StyleSelect>
                      </Col>

                      <Col span={12}>
                        <StyleSelect
                          size="small"
                          dropdownMatchSelectWidth={false}
                          className="my-year-select"
                          onChange={(newYear) => {
                            const now = value.clone().year(newYear);
                            onChange(now);
                          }}
                          value={String(year)}
                        >
                          {options}
                        </StyleSelect>
                      </Col>
                    </Row>
                  </div>
                );
              }}
              onPanelChange={onPanelChange}
            />
          </div>
        </CalendarDate>
      </Calendarcontent>
    </StyleWrapDrawer>
  );
}

export default Calendarer;
