
import styled from "styled-components";
import {Dropdown} from 'antd'



export const SectionMenu = styled.div`
    position : relative ;
    width : 100%;

`
export const SectionMenuSetting = styled.div`
    border-top: 1px solid #e6e8eb;
    position: absolute;
    width: 100%;
    
`
export const StyleDropdown = styled(Dropdown)`
    padding: 12px 24px;
    margin-left: -4px;
    border-radius: 4px;
    cursor: pointer;
    display :flex ;
    align-items : center ;
    .text {
        margin-left : 10px ;
        h3{
            margin-bottom : 0;
        }
    }
`;

export const Myorder = styled.div`
        display: flex;
    align-items: center;
    padding-left: 24px;
    padding-right: 6px;
    height : 48px;
    
    span{
        margin-left : 10px;
    }
`