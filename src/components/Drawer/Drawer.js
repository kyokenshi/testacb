import { Tabs } from "antd";
import { Header, StyleWrapDrawer, StyleTabs } from "./style";
import { Tag } from "antd";
import { AppleOutlined } from "@ant-design/icons";
import axios from "axios";
import React, { useState, useEffect } from "react";

/// img
import back from "../../assets/images/back.png";
import moment from "moment";

const { TabPane } = Tabs;

function DrawerComponent(props) {
  function callback(key) {
    console.log(key);
  }
  const token = localStorage.getItem("token");
  const [drawerv1, setdrawerv1] = useState([]);
  const [drawerv2, setdrawerv2] = useState([]);

  const getDrawerorder = () => {
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url:
        "cms/notification/my_notification?limit=20&page=1&generalType=[%22ORDER%22]",
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setdrawerv1(res.data.data.docs);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };
  const getDrawerseminor = () => {
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url: "cms/notification/my_notification?generalType=[%22EVENT%22]",
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setdrawerv2(res.data.data.docs);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };

  useEffect(() => {
    getDrawerorder();
    getDrawerseminor();
  }, []);

  ////////////////////////////////////////////////////////

  // console.log(props)
  function renderTitle() {
    return (
      <Header>
        <div className="title">
          <span>Thông báo</span>
        </div>
        <img src={back} className="imgback" onClick={props.onClose}></img>
      </Header>
    );
  }
  /////
  function renderTag(type) {
    switch (type) {
      case "DRAFT":
        return <Tag color="cyan">Nháp</Tag>;
      case "SUBMIT":
        return <Tag color="orange">Đặt hàng thành công</Tag>;
      case "PROCESSING":
        return <Tag color="orange">Đã tiếp nhận đơn hàng</Tag>;
      case "PROCEED":
        return <Tag color="geekblue">Bắt đầu giao hàng</Tag>;
      case "DELIVERING":
        return <Tag color="geekblue">Đang vận chuyển</Tag>;
      case "BACK_ORDER":
        return <Tag color="magenta">Đang chờ hàng</Tag>;
      case "CANCELED":
        return <Tag color="magenta">Đã hủy</Tag>;
      case "COMPLETED":
        return <Tag color="green">Thành công</Tag>;
      case "RETURN":
        return <Tag color="magenta">Trả hàng</Tag>;
      case "CREATE_ORDER_FOR_BUYER":
        return <Tag color="blue">Đặt hàng thành công</Tag>;
      case "CREATE_ORDER_FOR_ADMIN":
        return <Tag color="blue">Đặt hàng thành công</Tag>;
      case "ADMIN_CANCEL_ORDER":
        return <Tag color="volcano">Đơn hàng đã hủy</Tag>;
      case "ADMIN_RETURN_ORDER":
        return <Tag color="magenta">Trả đơn hàng</Tag>;
      case "ADMIN_PROCEED_ORDER":
        return <Tag color="magenta">Đang lấy hàng</Tag>;
      case "ADMIN_PROCESSING_ORDER":
        return <Tag color="warning">Đã tiếp nhận</Tag>;
      case "ADMIN_BACK_ORDER":
        return <Tag color="processing">Đang chờ hàng</Tag>;
      case "ADMIN_DELIVERING_ORDER":
        return <Tag color="warning">Đang giao hàng</Tag>;
      case "ADMIN_COMPLETE_ORDER":
        return <Tag color="success">Hoàn thành</Tag>;
      case "ADMIN_UPDATE_ORDER":
        return null;
      case "ADMIN_RE_ORDER":
        return null;
      default:
        return <Tag color="gold"></Tag>;
    }
  }

  function renderContent(type) {
    switch (type) {
      //orders
      case "ADMIN_SUBMIT_ORDER":
        return "Đã cập nhật trạng thái sang đơn mới.";

      case "ADMIN_PROCESSING_ORDER":
        return "Đơn hàng đang được chờ xử lý";

      case "ADMIN_PROCEED_ORDER":
        return "Đơn hàng đã được xử lý";

      case "ADMIN_DELIVERING_ORDER":
        return <span>Đơn hàng bắt đầu đi giao.</span>;
      case "ADMIN_COMPLETE_ORDER":
        return (
          <span>
            Đơn hàng đã được giao <b>thành công.</b>
          </span>
        );
      case "ADMIN_BACK_ORDER":
        return (
          <div>
            Đơn hàng đang chờ xử lý. Chúng tôi sẽ cập nhật và liên hệ cho Quý
            Bác sĩ
          </div>
        );
      case "ADMIN_CANCEL_ORDER":
        return (
          <span>
            Đã cập nhật trạng thái sang <b>đã hủy.</b>
          </span>
        );
      case "ADMIN_RETURN_ORDER":
        return (
          <span>
            Đã cập nhật trạng thái sang <b>trả hàng.</b>
          </span>
        );
      case "ADMIN_UPDATE_ORDER":
        return "Nơi không thuộc về";
      case "CREATE_ORDER_FOR_ADMIN":
        return <span>Đơn hàng được tạo thành công.</span>;
      case "ADMIN_RE_ORDER":
        return null;
      case "CREATE_ORDER_FOR_BUYER":
        return <span>Đơn hàng vừa được tạo.</span>;
      default:
        return "";
    }
  }

  function renderItemOrder() {
    return drawerv1.map((data) => (
      <div className="content" key={data.id}>
        <div className="content-header">
          <span className="text1">{data.order.orderId}</span>
          {renderTag(data.detail.type)}
        </div>
        <div className="content-detail">
          {/* {console.log(data.detail.type)} */}
          <span>{renderContent(data.type)}</span>
        </div>
        <div className="content-time">
          <span> 3 giờ trước </span>
        </div>
      </div>
    ));
  }

  function rederItemSeminor() {
    return drawerv2.map((item) => (
      <div className="content" key={item.id}>
        <div className="event-noti">
          <div className="event-noti-item">
            <div className="d-flex align-items-center mb-2">
              <AppleOutlined className="mr-2" />
              <div className="noti-calendar">
                {moment(item.createdAt).format("HH:mm, DD [thg]MM YYYY")}
              </div>
            </div>
            <div className="noti-content mb-1">
              <span>{item.detail.subject}</span>
            </div>
            <div className="noti-content2 mb-2">
              <span>{item.event.title}</span>
            </div>
            <div className="d-flex justify-content-between align-content-center noti-content3">
              <span className="tag-online">
                {item.event.type === "OFFLINE"
                  ? "Hội thảo gặp gỡ trực tiếp"
                  : "Hội thảo trực tuyến"}
              </span>
              <span className="time">
                {moment(item.createdAt).format("DD [thg]MM YYYY")}
              </span>
            </div>
          </div>
        </div>
      </div>
    ));
  }

  return (
    <StyleWrapDrawer
      title={renderTitle()}
      // headerStyle={{ color: "red" }}
      // placement={placement}
      // closable={false}
      closable={false}
      // onClose={props.onClose}
      visible={props.visible}
      width="100%"
      // key={placement}
    >
      <StyleTabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="Đơn hàng" key="1">
          <div className="read-mark">Đánh dấu tất cả đã đọc</div>

          {renderItemOrder()}
        </TabPane>

        <TabPane tab="Hội thảo" key="2">
          <div className="read-mark">Đánh dấu tất cả đã đọc</div>

          {rederItemSeminor()}
        </TabPane>
      </StyleTabs>
    </StyleWrapDrawer>
  );
}

export default DrawerComponent;
