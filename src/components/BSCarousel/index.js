import React from 'react';
import Slider from "react-slick";
import CardBS from '../BSCard'


function index(props) {
  
const settings = {
     dots: true,
     
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows	: true ,
     
    

    };

    return (
      <Slider {...settings}>
           {props.children} 
    </Slider>
    );
}

export default index;