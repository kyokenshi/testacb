import styled from "styled-components";

export const OrderSearch = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 12px 0 16px;
  .input {
    width: 100%;
  }
`;
