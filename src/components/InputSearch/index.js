import React, { useState } from "react";
import { Input } from "antd";

import { OrderSearch } from "./style";
import { SearchOutlined } from "@ant-design/icons";

function InputSearch(props) {
  return (
    <div>
      <OrderSearch>
        <Input
          size="large"
          className="input"
          placeholder="....."
          onChange={props.onChange}
          value={props.value}
          prefix={<SearchOutlined />}
        />
      </OrderSearch>
    </div>
  );
}

export default InputSearch;
