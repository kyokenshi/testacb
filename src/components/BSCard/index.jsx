import React, { useState, useEffect } from "react";
import Card1 from "../../assets/images/card1.png";
import { Card } from "./style";
import calendar from "../../assets/images/calendar.png";
import { Tag } from "antd";

import { CheckCircleOutlined } from "@ant-design/icons";

import { StyleButton } from "./style";

function CardBS({ openEventDetail, event = {} }) {
  console.log(event);
  const [isRegisted, setIsRegisted] = useState(false);

  return (
    <div>
      <Card onClick={() => openEventDetail()}>
        <img src={Card1} className="imgdt"></img>
        <div className="calendar">
          <div className="icon-calendar">
            <img src={calendar}></img>

            <span>{event?.timeTakesPlace}</span>
          </div>
          <Tag color={event.type === "OFFLINE" ? "processing" : "success"}>
            {event.type === "OFFLINE"
              ? "Hội thảo gặp gỡ trực tiếp"
              : "Hội thảo trực tuyến"}
          </Tag>
        </div>
        <span className="text-title">{event.title}</span>
        <hr></hr>
        <div className="buttondk">
          <StyleButton
            type="primary"
            icon={event.status !== "NONE" ? <CheckCircleOutlined /> : null}
            // ghi funtion vao trong ong click = arrow funtion
            onClick={() => setIsRegisted(true)}
          >
            {event.status === "NONE" ? "Đăng ký tham dự" : "Đã đăng ký"}
          </StyleButton>
        </div>
      </Card>
    </div>
  );
}

export default CardBS;
