import React from "react";
import PropTypes from "prop-types";
import { Button } from "antd";

function Index(props) {
  const { pagination, onPageChange } = props;
  const { page, limit } = pagination;

  function handlePageChange(newPage) {
    if (onPageChange) {
      onPageChange(newPage);
    }
  }

  return (
    <div>
      <Button disabled={page === 1} onClick={() => handlePageChange(page - 1)}>
        prev
      </Button>
      <button disabled={page > 1} onClick={() => handlePageChange(page + 1)}>
        next
      </button>
    </div>
  );
}

export default Index;
