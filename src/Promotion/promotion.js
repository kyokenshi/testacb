import React, { useState, useEffect } from "react";
import { Appp } from "../Khachhang/style";
import { PromotionPage, PromotionItem } from "./style";
import { Dropdown, Button, Menu } from "antd";

import Drwpromotion from "../components/Drawer/Drwpromotion/Drwpromition";

// import { OrderSearch } from "../Ordermanagement/style";
import OrderSearch from "../components/InputSearch";
import axios from "axios";

function Promotion() {
  const token = localStorage.getItem("token");
  const [promotions, setPromotions] = useState([]);
  const [visible, setVisible] = useState(false);
  const [showprotiondetail, setShowpromotiondetail] = useState([]);
  const [valueSearch, setValueSearch] = useState("");

  const getPromotion = () => {
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url: "cms/promotion/list_promotion_all_product?page=1&limit=20",
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setPromotions(res.data.data.docs);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };
  useEffect(() => {
    getPromotion();
    getPromotionDetail();
  }, []);

  function getPromotionDetail(id) {
    console.log(id);
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url: `cms/promotion/get_detail_promotion/${id}`,
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setShowpromotiondetail(res.data.data);
      })
      .catch((error) => {
        //api loi handle here!
      });
  }

  const menuItemPromotion = (item) => (
    <Menu>
      <Menu.Item
        onClick={() => {
          console.log(item);
          setVisible(true);
          getPromotionDetail(item._id);
        }}
      >
        <a>Xem chi tiết</a>
      </Menu.Item>
    </Menu>
  );
  return (
    <Appp>
      <PromotionPage>
        <div className="title-promotion">
          <h3>Thông tin khuyến mãi </h3>
        </div>
        <OrderSearch
          // onChange={handleChangeSearch}
          value={valueSearch}
        ></OrderSearch>
        <hr></hr>
        {promotions.map((item, key) => {
          // console.log(item);

          return (
            <PromotionItem key={key}>
              <div className="itempromotion">
                <img></img>
                <div className="ml-3">
                  <div className="text1 mb-3">{item.title}</div>
                  <div className="text2 mb-1">
                    <span>Sản phẩm : </span>
                    <b>{item.products.name}</b>
                  </div>
                  <div className="text3">
                    <span> HSD: 10/7/2020 - 17/07/2020</span>
                  </div>
                </div>
              </div>
              <Dropdown
                overlay={menuItemPromotion(item)}
                placement="bottomRight"
              >
                <Button>...</Button>
              </Dropdown>
            </PromotionItem>
          );
        })}
      </PromotionPage>
      <Drwpromotion
        visible={visible}
        onClose={() => setVisible(false)}
        promotions={showprotiondetail}
      />
    </Appp>
  );
}

export default Promotion;
