import styled from "styled-components";
import { Card ,Pagination} from "antd";

export const Ordermanagement = styled.div`
  padding: 16px;
`;
export const OrderSearch = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 12px 0 16px;
  .input {
    width: calc(100% - 100px);
  }
`;
export const StyleCard = styled(Card)`
  padding: 0 16px;
  width: 100%;
  border-radius: 4px;
  border: 1px solid #e6e8eb;
  margin-bottom: 20px;
  box-shadow: 1px 1px 1px hsla(0, 0%, 80%, 0.6);
  .header-card {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 8px;
    .filer-card {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
    span {
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 16px;
      color: #6b778c;
    }

    Button {
      background-color: white;
      border-color: red;
    }
  }

  .tag {
    border-radius: 4px;
  }

  .applicationcreator {
    margin: 10px 0 14px;
    font-size: 14px;
    color: #172b4d;
  }
  .time {
    font-size: 12px;
    line-height: 16px;
    color: #6b778c;
    border-top: 1px solid #e6e8eb;
    padding: 8px 0;
  }
`;

export const StylePagination = styled(Pagination)`
            display: flex;
    justify-content: flex-end;
    `