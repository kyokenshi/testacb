import React, { useState, useEffect } from "react";
import { Appp } from "../Khachhang/style";
import CardBS from "../components/BSCard";
import { HomeEvent, StyleSection } from "../eventPage/style";
import Drawereventdetail from "../eventPage/eventDetail/eventDetail";
import axios from "axios";

function EventsPage() {
  const [visible, setVisible] = useState(false);

  const [listevent, setListevent] = useState([]);
  const token = localStorage.getItem("token");

  const [eventdetail, setEventdetail] = useState([]);

  const handleClickCard = (url) => {
    console.log(url);
    setVisible(true);

    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url: `event/event-detail-client/${url}`,
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setEventdetail(res.data.data);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };
  ///////
  const getEventList = () => {
    // console.log(token);
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url: "event/list-by-doctor?page=1&limit=10",
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res.data.data.docs);
        setListevent(res.data.data.docs);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };
  useEffect(() => {
    getEventList();
  }, []);

  return (
    <Appp>
      <HomeEvent>
        <div className="textHoithao">Hội thảo </div>
      </HomeEvent>
      <StyleSection>
        <div className="text30day">hội thảo trong vòng 30 ngày</div>
        {listevent.map((event, key) => {
          console.log(event);
          const url = event.url;

          return (
            <CardBS
              key={key}
              event={event}
              openEventDetail={() => handleClickCard(url)}
            ></CardBS>
          );
        })}
      </StyleSection>

      <Drawereventdetail
        visible={visible}
        onClose={() => setVisible(false)}
        data={[]}
        eventdetail={eventdetail}
      />
    </Appp>
  );
}

export default EventsPage;
