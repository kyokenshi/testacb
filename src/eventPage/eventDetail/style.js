import styled from "styled-components";
import "antd/dist/antd.css";

export const EventDetail = styled.div`
  .imgcard {
    width: 100%;
    margin-bottom: 18px;
  }
`;
export const Detailcontent = styled.div`
  padding: 0 16px;
  padding-bottom: 16px;
  .event-detail-calendar {
    display: flex;
    align-items: center;
  }
`;
// export const DetailcontentCollapse = styled.div`
//   padding: 0 16px;
// `;
